﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script Display Menu Pause

public class PauseMenu : MonoBehaviour
{

    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;
    public GameObject PP_Pause;
    public Memo_Game Script;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape)){
             if (GameIsPaused)
             {
                 Resume();
             }else{
                 Pause();
             }
         }
    }

    public void Resume()
    {
        AudioListener.volume = 1f;
        Script.OnPause = false;
        pauseMenuUI.SetActive(false);
        PP_Pause.SetActive(false);

        Time.timeScale = 1f;
        GameIsPaused = false;

    }

    void Pause()
    {
        Script.OnPause = true;
        AudioListener.volume = 0f;
        pauseMenuUI.SetActive(true);
        PP_Pause.SetActive(true);

        Time.timeScale = 0f;
        GameIsPaused = true;
    }
}
