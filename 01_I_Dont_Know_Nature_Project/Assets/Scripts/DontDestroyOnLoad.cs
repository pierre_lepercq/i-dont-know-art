﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Script to not destroy Gameobjects with tag "Not_Destroy" between each scenes

public class DontDestroyOnLoad : MonoBehaviour
{
    private GameObject[] Not_Destroy;

    private void Awake()
    {
        Not_Destroy = GameObject.FindGameObjectsWithTag("Not_Destroy");

        //if object already exists, destroy it
        if (Not_Destroy.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this);

    }
}
