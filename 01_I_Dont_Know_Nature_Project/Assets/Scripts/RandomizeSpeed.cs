﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script to randomize the speed of the animator

public class RandomizeSpeed : MonoBehaviour
{
    public GameObject Object;

    public float minSpeed;
    public float maxSpeed;

    // Start is called before the first frame update
    void Start()
    {
        Object.GetComponent<Animator>().speed = Random.Range(minSpeed, maxSpeed);
    }
}
