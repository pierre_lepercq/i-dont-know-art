﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Play_Anims : MonoBehaviour
{
    Animator life_Animator;
    Animator Score_Animator;
    Animator Item_Animator;
    Animator Compteur_Animator;
    Animator Nbr_Find_Group_Animator;
    Animator Texts_Animator;
    //bool loose_life;

    // Start is called before the first frame update
    void Start()
    {
        life_Animator = gameObject.GetComponent<Animator>();
        Score_Animator = gameObject.GetComponent<Animator>();
        Item_Animator = gameObject.GetComponent<Animator>();
        Compteur_Animator = gameObject.GetComponent<Animator>();
        Nbr_Find_Group_Animator = gameObject.GetComponent<Animator>();
        Texts_Animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        /* if (Input.GetKeyDown(KeyCode.A))
            loose_life = true;

        if (loose_life == false)
        {
            life_Animator.SetBool("loose_life", false);
        }
            
        if (loose_life == true)
        {
            life_Animator.SetBool("loose_life", true);
        }  */

    }

    public void Play_Life_Anim()
    {
        life_Animator.SetBool("loose_life", true);
    }

    public void Play_Score_Anim()
    {
        Score_Animator.SetBool("Score+", true);
    }

    public void Stop_Score_Anim()
    {
        Score_Animator.SetBool("Score+", false);
    }

    public void Play_Item_Anim()
    {
        Item_Animator.SetBool("Grow", false);
    }

    public void Open_Compteur_Anim()
    {
        Compteur_Animator.SetBool("Open", true);

    }

    public void Close_Compteur_Anim()
    {
        Compteur_Animator.SetBool("Open", false);
    }

    public void Apparition_Compteur_Anim()
    {
        Compteur_Animator.SetBool("Disparition", false);

    }

    public void Disparition_Compteur_Anim()
    {
        Compteur_Animator.SetBool("Disparition", true);

    }

    public void Apparition_Texts_Anim()
    {
        Compteur_Animator.SetBool("Apparition", true);

    }

    public void Disparition_Texts_Anim()
    {
        Compteur_Animator.SetBool("Apparition", false);

    }

}
