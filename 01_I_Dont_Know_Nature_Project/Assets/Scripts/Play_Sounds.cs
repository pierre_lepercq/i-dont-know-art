﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Play_Sounds : MonoBehaviour
{
    public GameObject[] Buttons;
    AudioSource Sound1;

    void Start()
    {
        Sound1 = Buttons[0].GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            Sound1.Play(0);
        }
    }
}
