﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class Memo_Game : MonoBehaviour
{
    //  Public

    public GameObject[] Leafs;

    public GameObject[] listOfLife;

    public GameObject[] Pop_Decorations;

    public AudioClip[] Sounds;

    public Play_Anims[] Anim_Score;

    public Play_Anims[] Anim_Compteur;

    public Play_Anims[] Anim_Texts;

    public GameObject PP_Profils;

    public GameObject Pause;

    public Vector3 PositionPattern;

    

    public int life = 1;

    public int delay = 50;
    public int delay_display = 600;

    public bool OnPause;

    public Text[] Level_text;
    public Text Memorize_text;
    public Text Play_text;

    public Text score_text;
    public Text NbrToFind_text;

    //  Private

    private int level_id;
    private int NbrToFind;

    private int cptr;
    private int cptr_display;

    private int score;
    private int highScore;

    private bool OnPlay;
    private bool Dead;

    private int current_id;

    private List<int> list_memory = new List<int>();

    private GameObject currentPatern;

    private new AudioSource audio;

    private Shake shake;

    //  -------------------------------------------------------------   //

    //  Other Functions

    //  -------------------------------------------------------------   //

    void succes()
    {
        cptr = delay;

        audio.PlayOneShot(Sounds[0], 1f);

        level_id++;
        current_id++;

        //  Si la sequence est complete
        if (current_id == list_memory.Count)
        {

            Anim_Compteur[0].Close_Compteur_Anim();
            Anim_Compteur[1].Close_Compteur_Anim();
            Anim_Compteur[2].Disparition_Compteur_Anim();

            if (level_id % 10 == 0)
            {
                cptr_display = (int)(delay_display);
            }
            else
            {
                cptr_display = (int)(delay_display * 0.5);
            }

            score += list_memory.Count * 10;
            score_text.text = score.ToString();

            Anim_Score[0].Play_Score_Anim();
            Anim_Score[1].Play_Score_Anim();
            audio.PlayOneShot(Sounds[3], 1f);

            Pop_Decorations[list_memory.Count - 1].SetActive(true);

            list_memory.Add(Random.Range(0, 6));

            current_id = 0;
            OnPlay = false;


        }

        else
        {
            Anim_Score[0].Stop_Score_Anim();
            Anim_Score[1].Stop_Score_Anim();
        }

        NbrToFind = list_memory.Count - current_id;
        NbrToFind_text.text = NbrToFind.ToString();
    }

    //  -------------------------------------------------------------   //
    void defeat()
    {
        cptr_display = (int)(delay_display * 0.5);

        //  Si le joueur n a plus de vie
        if (life <= 0)
        {
            if (score > highScore)
            {
                PlayerPrefs.SetInt("HighScore", score);
            }

            Debug.Log("Dead");
            Dead = true;
        }

        // Si le joueur a encore 1 vie ou plus
        else
        {
            life--;

            current_id = 0;
            OnPlay = false;

            NbrToFind = list_memory.Count;
            NbrToFind_text.text = NbrToFind.ToString();

            shake.CamShake();
            audio.PlayOneShot(Sounds[1], 2f);

            listOfLife[life].GetComponent<Play_Anims>().Play_Life_Anim();
        }
    }

    //  -------------------------------------------------------------   //
    void Awake()
    {
        audio = GetComponent<AudioSource>();
        shake = GameObject.FindGameObjectWithTag("ScreenShake").GetComponent<Shake>();
        PP_Profils.GetComponent<PP_Anim>().PP_Pause.SetActive(true);

        Level_text[0].enabled = false;
        Level_text[1].enabled = false;
        Level_text[2].enabled = false;
        Level_text[3].enabled = false;

        Memorize_text.enabled = false;
        Play_text.enabled = false;
    }

    //  -------------------------------------------------------------   //
    void Start()
    {
        //Initialisation
        current_id = 0;
        OnPlay = false;
        OnPlay = false;

        cptr = 0;
        cptr_display = delay_display;

        score = 0;
        highScore = PlayerPrefs.GetInt("HighScore", 0);

        list_memory.Add(Random.Range(0, 6));
        NbrToFind = list_memory.Count;

        //Display
        score_text.text = 0.ToString();
        NbrToFind_text.text = NbrToFind.ToString();
        
    }

    //  -------------------------------------------------------------   //
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            Pause.GetComponent<PauseMenu>();
        }

        //Quick Restart Level
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("Level");
        }


        else if (OnPause == false)
        {
            //  Si le jeu est en mode play
            if (OnPlay)
            {
                shake.Idle();

                //  Affichage
                if (cptr_display > 0)
                {
                    if (cptr_display == (int)(delay_display * 0.3))
                    {
                        Play_text.enabled = true;
                        Anim_Texts[3].Apparition_Texts_Anim();
                        audio.PlayOneShot(Sounds[4], 1.5f);
                        Anim_Compteur[0].Open_Compteur_Anim();
                        Anim_Compteur[1].Open_Compteur_Anim();
                        Anim_Compteur[2].Apparition_Compteur_Anim();
                    }
                    if (cptr_display == (int)(delay_display * 0.1))
                    {
                        Play_text.enabled = false;
                        Anim_Texts[3].Disparition_Texts_Anim();
                    }
                }

                //  Attente d'une touche
                else if (Input.anyKey && cptr <= 0)
                {

                    if (Input.GetKey(KeyCode.UpArrow) && list_memory[current_id] == 0)
                    {
                        succes();
                    }

                    else if (Input.GetKey(KeyCode.DownArrow) && list_memory[current_id] == 1)
                    {
                        succes();
                    }

                    else if (Input.GetKey(KeyCode.LeftArrow) && list_memory[current_id] == 2)
                    {
                        succes();
                    }

                    else if (Input.GetKey(KeyCode.RightArrow) && list_memory[current_id] == 3)
                    {
                        succes();
                    }

                    else if (Input.GetKey(KeyCode.Space) && list_memory[current_id] == 4)
                    {
                        succes();
                    }

                    else if (Input.GetMouseButton(0) && list_memory[current_id] == 5)
                    {
                        succes();
                    }

                    else
                    {
                        if (cptr == 0)
                        {
                            defeat();
                        }
                    }

                }
            }


            //  Affichage de la serie a memoriser
            else if (cptr_display <= 0)
            {

                if (cptr == 0)
                {
                    if (level_id < 50)
                    {
                        cptr = 100 - level_id;
                    }
                    else
                    {
                        cptr = 50;
                    }

                    //Apparition Motif
                    currentPatern = Instantiate(Leafs[list_memory[current_id]], PositionPattern, Quaternion.identity);

                    if (level_id > 12 && Random.value < 0.05f * (level_id % 10))
                    {
                        if (Random.value > 0.5f)
                            currentPatern.transform.Find("Color").GetComponent<SpriteRenderer>().enabled = false;
                        else
                            currentPatern.transform.Find("Line").GetComponent<SpriteRenderer>().enabled = false;
                    }

                    current_id++;

                }

                else if (cptr == 5)
                {
                    Destroy(currentPatern);
                }

                else if (cptr == 1)
                {
                    if (current_id == list_memory.Count)
                    {
                        current_id = 0;
                        OnPlay = true;

                        cptr_display = (int)(delay_display * 0.35);
                    }
                }

            }

            //  Delay si la partie est terminee
            if (Dead == true)
            {

                Debug.Log("GameOver");

                PP_Profils.GetComponent<PP_Anim>().PP_Pause.SetActive(false);

                if (score > 500)
                    SceneManager.LoadScene("Scenes/Win");
                else
                    SceneManager.LoadScene("Scenes/Dead");

                /*if (cptr_display == (int)(delay_display * 0.05))
                {
                    PP_Profils.GetComponent<PP_Anim>().PP_Pause.SetActive(false);

                    if (score > 500)
                        SceneManager.LoadScene("Scenes/Win");
                    else
                        SceneManager.LoadScene("Scenes/Dead");
                }*/
            }

            //  Affichage du niveau
            else
            {
                //  Display
                //  Affichage Stage
                if (cptr_display == (int)(delay_display * 0.95))
                {
                    PP_Profils.GetComponent<PP_Anim>().PP_Pause.SetActive(true);

                    Level_text[0].enabled = true;

                    Anim_Texts[0].Apparition_Texts_Anim();

                    audio.PlayOneShot(Sounds[2], 1f);

                    Level_text[1].text = ((level_id - level_id % 10) / 10 + 1).ToString();

                    Anim_Texts[1].Apparition_Texts_Anim();

                    Level_text[1].enabled = true;

                }
                //  Affichage Sequence
                /*else if (cptr_display == (int)(delay_display * 0.7))
                {
                    Level_text[2].enabled = true;

                    Level_text[3].text = (level_id % 10 + 1).ToString();

                    Level_text[3].enabled = true;
                }*/
                //  Disparition
                else if (cptr_display == (int)(delay_display * 0.55))
                {
                    Level_text[0].enabled = false;
                    Anim_Texts[0].Disparition_Texts_Anim();
                    Level_text[1].enabled = false;
                    Anim_Texts[1].Disparition_Texts_Anim();
                    //Level_text[2].enabled = false;
                    //Level_text[3].enabled = false;
                }

                //Affichage du flou
                /* else if (cptr_display == (int)(delay_display * 0.25))
                 {
                     PP_Profils.GetComponent<PP_Anim>().PP_Pause.SetActive(true);
                 }*/

                //  Desactivation du Flou
                else if (cptr_display == (int)(delay_display * 0.5))
                {
                    PP_Profils.GetComponent<PP_Anim>().PP_Pause.SetActive(false);
                }

                //  Affichage Memorize
                else if (cptr_display == (int)(delay_display * 0.45))
                {
                    Memorize_text.enabled = true;
                    Anim_Texts[2].Apparition_Texts_Anim();

                }
                //  Disparition Memorize
                else if (cptr_display == (int)(delay_display * 0.2))
                {
                    Memorize_text.enabled = false;
                    Anim_Texts[2].Disparition_Texts_Anim();
                }
                

            }

            //  Update Compteurs
            if (cptr_display >= 1)
            {
                cptr_display--;
            }

            if (cptr >= 1)
            {
                cptr--;
            }
        }
    }
}
