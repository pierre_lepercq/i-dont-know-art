﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class TryDisplay : MonoBehaviour
{
    public int cptr;
    public int delay;
    public GameObject[] listOfColors;

    public GameObject Item1;
    public GameObject Item2;
    public GameObject Item3;
    public GameObject Item4;
    public GameObject Item5;
    public GameObject Item6;

    Animator Item1_Animator;
    Animator Item2_Animator;
    Animator Item3_Animator;
    Animator Item4_Animator;
    Animator Item5_Animator;
    Animator Item6_Animator;

    //AudioSource audioData;

    private bool up, down, right, left, mouse, space = false;

    // Start is called before the first frame update
    void Start()
    {
        cptr = delay;

        Item1_Animator = Item1.GetComponent<Animator>();
        Item2_Animator = Item2.GetComponent<Animator>();
        Item3_Animator = Item3.GetComponent<Animator>();
        Item4_Animator = Item4.GetComponent<Animator>();
        Item5_Animator = Item5.GetComponent<Animator>();
        Item6_Animator = Item6.GetComponent<Animator>();

        //audioData = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Compteur =" + cptr);

        //Item1
        if (Input.GetKey(KeyCode.UpArrow))
        {
            Item1_Animator = Item1.GetComponent<Animator>();
            listOfColors[0].GetComponent<Image>().enabled = true;
            Item1_Animator.SetBool("Grow", true);
            //audioData.Play(0);
            Debug.Log("compteur = " + cptr);
            up = true;

        }

        else
        {
            listOfColors[0].GetComponent<Image>().enabled = false;
            Item1_Animator.SetBool("Grow", false);
        }


        //Item2
        if (Input.GetKey(KeyCode.DownArrow))
        {
            Item2_Animator = Item2.GetComponent<Animator>();
            listOfColors[1].GetComponent<Image>().enabled = true;
            Item2_Animator.SetBool("Grow", true);
            //audioData.Play(0);
            down = true;


        }
        else
        {
            listOfColors[1].GetComponent<Image>().enabled = false;
            Item2_Animator.SetBool("Grow", false);
        }

        //Item3
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Item3_Animator = Item3.GetComponent<Animator>();
            listOfColors[2].GetComponent<Image>().enabled = true;
            Item3_Animator.SetBool("Grow", true);
            //audioData.Play(0);
            left = true;
        }
        else
        {
            listOfColors[2].GetComponent<Image>().enabled = false;
            Item3_Animator.SetBool("Grow", false);
        }

        //Item4
        if (Input.GetKey(KeyCode.RightArrow))
        {
            listOfColors[3].GetComponent<Image>().enabled = true;
            Item4_Animator.SetBool("Grow", true);
            //audioData.Play(0);
            right = true;
        }
        else
        {
            listOfColors[3].GetComponent<Image>().enabled = false;
            Item4_Animator.SetBool("Grow", false);
        }

        //Item5
        if (Input.GetKey(KeyCode.Space))
        {
            listOfColors[4].GetComponent<Image>().enabled = true;
            Item5_Animator.SetBool("Grow", true);
            //audioData.Play(0);
            space = true;
        }
        else
        {
            listOfColors[4].GetComponent<Image>().enabled = false;
            Item5_Animator.SetBool("Grow", false);
        }

        //Item6
        if (Input.GetMouseButton(0))
        {
            listOfColors[5].GetComponent<Image>().enabled = true;
            Item6_Animator.SetBool("Grow", true);
            //audioData.Play(0);
            mouse = true;
        }
        else
        {
            listOfColors[5].GetComponent<Image>().enabled = false;
            Item6_Animator.SetBool("Grow", false);
        }

        if (up&down&right&left&mouse&space == true)
        {
            Debug.Log("check butons ok");

            if (cptr == 0)
            {
                SceneManager.LoadScene("Scenes/Level");
            }

            else
            {
                cptr--;
            }
        }

    }
}
